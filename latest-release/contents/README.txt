WE ARE PROPHET      
==============

Table of Contents
-----------------
1. How To Play
2. Links
3. Installation Instructions


How To Play
-----------
Your goal is it to control 11 fields on the screen!

Each player starts with one prophet (the tall old guy with the staff).
Click on your character and then click on the part of the screen you want to talk to.
Walk up to the people around the map, to talk with them and add them to your team.
The Prophet of the other player can't be converted.

You can walk around the map with each character you converted!
Use them to add more characters to your team with them, including members of the enemy team!
Now conquer the fields of the map by standing on them for a certain amount of time!
If the enemy team has already conquered a field, you'll need a bit longer to do so.

Here are the 5 different character types you can control:
- the Prophet: is slow with adding people to your team and walking, but can't be converted himself
- the child: moves very fast, but switches sides quickly
- the young man: walks with a medium speed and also has medium power
- the old, fat man: is slow, but strong in resisting conversion
- the gospel lady: is evern slower, but the strongest in converting people

If there is a crowd of characters, trying to convert each other to their team, their attack is being added.
Get your weaker characters some help, when they are under attack!

Have Fun playing and drop us a note if you enjoyed it! ;)
(or if you want some pro developers to play against)


Links
-----
http://globalgamejam.org/2014/games/prophet

The source of this project is publicly available at: https://bitbucket.org/weareprophet/
Modders are welcome (please host your own server).


Installation Instructions
-------------------------
First of all extract the contents of prophet.zip using a programm like 7zip or Winrar.
Start the game by double-clicking either on We_Are_Prophet.exe or We_Are_Prophet.sh.
Grab some friends and battle each other :)

Linux Users: You need the Original JDK (Java Development Kit) from Java, not OpenJDK.
You can use the following code to get it:

sudo apt-get install openjdk-7-jdk
