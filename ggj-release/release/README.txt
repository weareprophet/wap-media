WE ARE PROPHET      
==============
http://globalgamejam.org/2014/games/prophet

This Real-Time-Online-Multiplayergame is about persuading other people to not see the things as they are, but to see the things as we are. You battle another human player for becoming the greatest persuader. To do so, you try to spread your influence to the given sectors. But players can pull neutral people to their sides too... It's your choice what tactic you will use to spread your influence to everyone.

The source of this project is publicly available at: https://bitbucket.org/weareprophet/

Instructions
------------
First, unzip the prophet.zip - it contains two sub directories "server" and "client". If you already know a server address, you don't need to start one yourself and can head over to the "Starting the client" section. Otherwise, start a server first.

Starting the server
-------------------
To start the server, execute the following command in your shell in the server directory:

java -jar server-VERSION-jar-with-dependencies.jar PORT 

Please replace VERSION with the version you have downloaded and PORT with a server port, e.g. 8090.

Starting the client
-------------------
To start the client, execute the following command in your shell in the client directory:

java -Djava.library.path=natives -cp "client-VERSION-jar-with-dependencies.jar" de.ggj14.wap.WapGame SERVER_URL

Please replace VERSION with the version you have downloaded and SERVER_URL with the URL and port of your server, e.g. "http://myserver.net:8090/prophet". You can omit this argument to use the default server, if running.

